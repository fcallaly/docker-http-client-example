## Example Steps to deploy the nginx container to openshift

NOTE: In all commands below replace "frank" with your first name!

1. Send your docker image to the training docker registry at: dockerreg.conygre.com:5000

  - Tag your image as destined for the training docker registry:
    `docker tag http-example/nginx:1.0.0 dockerreg.conygre.com:5000/http-example/frank-nginx:1.0.0`

  - Push your tagged image to the training dockerregistry:
    `docker push dockerreg.conygre.com:5000/http-example/frank-nginx:1.0.0`

2. Login to openshift with the same credentials as you used for the web interface (username: studentXX, password: c0nygre)
   e.g. `oc login dev1.conygre.com:8443`

3. Tell openshift to deploy your container from the training docker registry:
   `oc new-app --docker-image dockerreg.conygre.com:5000/http-example/frank-nginx:1.0.0`

4. Tell openshift to allow access to your container from the outside:
   `oc expose svc/frank-nginx`

5. Your container should now be accessible from the internet at a url similar to:
   http://frank-nginx-test-project.dev2.conygre.com
